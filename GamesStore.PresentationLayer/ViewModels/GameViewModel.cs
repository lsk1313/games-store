﻿using System.Collections.Generic;

namespace GamesStore.PresentationLayer.ViewModels
{
    public class GameViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Price { get; set; }

        public string ImageName { get; set; }

        public string ImageSource{ get; set; }

        public CategoryViewModel[] SelectedCategories { get; set; }

    }
}
