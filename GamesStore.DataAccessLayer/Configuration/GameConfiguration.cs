﻿using GamesStore.Entities.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GamesStore.DataAccessLayer.Configuration
{
    public class GameConfiguration : IEntityTypeConfiguration<Game>
    {
        public void Configure(EntityTypeBuilder<Game> builder)
        {
            builder.HasData(new Game
            {
                Id = 1,
                Name = "Counter-Strike: Global Offensive",
                Price = 2.5,
                ImageName = "csgo",
                ImageSource = null,
                Description = "Counter-Strike: Global Offensive (CS: GO) " +
                              "expands upon the team-based action gameplay that " +
                              "it pioneered when it was launched 19 years ago. " +
                              "CS: GO features new maps, characters, weapons, and " +
                              "game modes, and delivers updated versions of the " +
                              "classic CS content (de_dust2, etc.)."
            });
        }
    }
}
