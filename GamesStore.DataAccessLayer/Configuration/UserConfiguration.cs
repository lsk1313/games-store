﻿using GamesStore.Entities.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GamesStore.DataAccessLayer.Configuration
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasData(new User
            {
                Id = 1,
                Password = "GyIZmr/EVfouuXlePiUwmD4Hjk3DiA6uDLEutYPOEtw="
            });
        }
    }
}
