﻿using GamesStore.Entities.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GamesStore.DataAccessLayer.Configuration
{
    public class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.HasData(new Category
            {
                Id = 1,
                CategoryName = "Action"
            }, new Category
            {
                Id = 2,
                CategoryName = "Adventure"
            }, new Category
            {
                Id = 3,
                CategoryName = "Indie"
            }, new Category
            {
                Id = 4,
                CategoryName = "Massively Multiplayer"
            }, new Category
            {
                Id = 5,
                CategoryName = "Casual"
            }, new Category
            {
                Id = 6,
                CategoryName = "Racing"
            }, new Category
            {
                Id = 7,
                CategoryName = "RPG"
            }, new Category
            {
                Id = 8,
                CategoryName = "Simulation"
            }, new Category
            {
                Id = 9,
                CategoryName = "Sports"
            }, new Category
            {
                Id = 10,
                CategoryName = "Strategy"
            });
        }
    }
}
