﻿using GamesStore.DataAccessLayer.Repository;
using GamesStore.RepositoryInterfaces.ObjectInterfaces;
using GamesStore.RepositoryInterfaces.UoW;

namespace GamesStore.DataAccessLayer.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(string connectionString)
        {
            UserRepository = new UserRepository(connectionString, "Users");

            GameRepository = new GameRepository(connectionString, "Games");

            CategoryRepository = new CategoryRepository(connectionString, "Categories");

        }

        public IUserRepository UserRepository { get; }
        public IGameRepository GameRepository { get; }
        public ICategoryRepository CategoryRepository { get; }

    }
}
