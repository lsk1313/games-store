﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using GamesStore.DataAccessLayer.GenericRepository;
using GamesStore.Entities.Domain;
using GamesStore.RepositoryInterfaces.ObjectInterfaces;

namespace GamesStore.DataAccessLayer.Repository
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(string connectionString, string tableName) : base(connectionString, tableName)
        {
        }

        public async Task<IEnumerable<User>> GetUsers() => await GetEntities();

        public async Task DeleteUser(int id) => await DeleteEntity(id);

        public async Task<User> GetUserById(int id) => await GetEntityById(id);

        public async Task<User> GetUserByLogin(string login)
        {
            using var connection = await OpenConnection();

            return await connection
                .QuerySingleOrDefaultAsync<User>("SELECT * FROM Users WHERE Login = @Login",
                    new { Login = login });
        }

        public async Task<User> GetUserByLoginAndPassword(string login, string password)
        {
            using var connection = await OpenConnection();

            return await connection
                .QuerySingleOrDefaultAsync<User>("SELECT * FROM Users WHERE Login = @Login AND Password = @Password",
                    new { Login = login, Password = password });
        }

        public async Task UpdateUser(User user) => await Update(user);

        public async Task InsertUser(User user) => await Insert(user);
    }
}

