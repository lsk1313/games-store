﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using GamesStore.DataAccessLayer.GenericRepository;
using GamesStore.Entities.Domain;
using GamesStore.RepositoryInterfaces.ObjectInterfaces;

namespace GamesStore.DataAccessLayer.Repository
{
    public class GameRepository : GenericRepository<Game>, IGameRepository
    {
        public GameRepository(string connectionString, string tableName) : base(connectionString, tableName)
        {
        }

        public async Task DeleteGame(int id) => await DeleteEntity(id);

        public async Task<Game> GetGameByName(string name) => await GetEntityByName(name);

        public async Task<Game> GetGameById(int id) => await GetEntityById(id);

        public async Task<IEnumerable<Game>> GetGames()
        {
            using var connection = await OpenConnection();

            const string query =
                "SELECT * FROM Games g " +
                "LEFT JOIN CategoryGame cg ON g.Id = cg.GameId " +
                "LEFT JOIN Categories c ON c.Id = cg.CategoryId";

            var games = await connection.QueryAsync<Game, Category, Game>(query, (game, category) =>
            {
                game.Categories.Add(category);

                return game;
            });

            return games.GroupBy(g => g.Id).Select(g =>
            {
                var combinedGame = g.First();
                combinedGame.Categories = g.Select(game => game.Categories.Single()).ToList();

                return combinedGame;
            });
        }

        public async Task UpdateGame(Game game)
        {
            await Update(game);

            await DeleteCategoryGame(game.Id);

            if (game.Categories.Count == 0) return;

            await InsertCategoryGame(game, game.Id);
        }

        public async Task InsertGame(Game game)
        {
            var id = await Insert(game);

            if (game.Categories.Count == 0) return;

            await InsertCategoryGame(game, id);
        }

        private async Task InsertCategoryGame(Game game, int id)
        {
            using var connection = await OpenConnection();

            var query = new StringBuilder("INSERT INTO CategoryGame ([CategoryId], [GameId]) VALUES ");

            foreach (var category in game.Categories)
            {
                query.Append("(").Append(category.Id).Append(",").Append(id).Append(")").Append(",");
            }

            query.Remove(query.Length - 1, 1);

            await connection.ExecuteAsync(query.ToString());
        }

        private async Task DeleteCategoryGame(int id)
        {
            using var connection = await OpenConnection();

            const string query = "DELETE FROM CategoryGame " +
                                 "WHERE GameId = @GameId";

            await connection.ExecuteAsync(query, new { GameId = id });
        }
    }
}
