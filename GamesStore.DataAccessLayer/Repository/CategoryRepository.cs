﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GamesStore.DataAccessLayer.GenericRepository;
using GamesStore.Entities.Domain;
using GamesStore.RepositoryInterfaces.ObjectInterfaces;

namespace GamesStore.DataAccessLayer.Repository
{
    public class CategoryRepository : GenericRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(string connectionString, string tableName) : base(connectionString, tableName)
        {
        }

        public async Task<IEnumerable<Category>> GetCategories() => await GetEntities();

        public async Task DeleteCategory(int id) => await DeleteEntity(id);

        public async Task<Category> GetCategory(int id) => await GetEntityById(id);

        public async Task UpdateCategory(Category category) => await Update(category);

        public async Task InsertCategory(Category category) => await Insert(category);
       
    }
}
