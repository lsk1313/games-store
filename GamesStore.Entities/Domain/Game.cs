﻿using System.Collections.Generic;
using System.ComponentModel;

namespace GamesStore.Entities.Domain
{
    public class Game
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public double Price { get; set; }

        public string ImageName { get; set; }

        public string ImageSource { get; set; }

        [Description("Ignore")]
        public ICollection<Category> Categories { get; set; }

        public Game()
        {
            Categories ??= new List<Category>();
        }

    }
}
