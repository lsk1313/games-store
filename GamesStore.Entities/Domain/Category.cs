﻿using System.Collections.Generic;
using System.ComponentModel;

namespace GamesStore.Entities.Domain
{
    public class Category
    {
        public int Id { get; set; }

        public string CategoryName { get; set; }

        [Description("Ignore")]
        public ICollection<Game> Games { get; set; }

        public Category()
        {
            Games ??= new List<Game>();
        }
    }
}