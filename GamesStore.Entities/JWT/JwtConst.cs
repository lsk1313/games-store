﻿
namespace GamesStore.Entities.JWT
{
    public static class JwtConst
    {
        public const string ISSUER = "https://localhost:5001/";

        public const string AUDIENCE = "https://localhost:5001/";

        public const string KEY = "Q2JlBpy235jl8Au6mqxsNzluF";

        public const int LIFETIME = 15;
    }
}