﻿using System.Collections.Generic;

namespace GamesStore.Entities.DataTransferObjects
{
    public class CategoryDto
    {
        public int Id { get; set; }

        public string CategoryName { get; set; }

        public ICollection<GameDto> Games { get; set; }

        public CategoryDto()
        {
            Games ??= new List<GameDto>();
        }
    }
}
