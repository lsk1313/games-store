﻿using System.Collections.Generic;

namespace GamesStore.Entities.DataTransferObjects
{
    public class GameDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public double Price { get; set; }

        public string ImageName { get; set; }

        public string ImageSource { get; set; }

        public ICollection<CategoryDto> Categories { get; set; }

        public GameDto()
        {
            Categories ??= new List<CategoryDto>();
        }
    }
}
