﻿using System;
using System.Threading.Tasks;
using GamesStore.RepositoryInterfaces.ObjectInterfaces;

namespace GamesStore.RepositoryInterfaces.UoW
{
    public interface IUnitOfWork 
    {
        IUserRepository UserRepository { get; }

        IGameRepository GameRepository { get; }

        ICategoryRepository CategoryRepository { get; }

    }
}
