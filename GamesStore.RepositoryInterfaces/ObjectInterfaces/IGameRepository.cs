﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GamesStore.Entities.Domain;

namespace GamesStore.RepositoryInterfaces.ObjectInterfaces
{
    public interface IGameRepository
    {
        Task<IEnumerable<Game>> GetGames();

        Task DeleteGame(int id);

        Task<Game> GetGameByName(string name);

        Task<Game> GetGameById(int id);

        Task UpdateGame(Game game);

        Task InsertGame(Game game);
    }
}