﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GamesStore.Entities.Domain;

namespace GamesStore.RepositoryInterfaces.ObjectInterfaces
{
    public interface IUserRepository
    {
        Task<IEnumerable<User>> GetUsers();

        Task DeleteUser(int id);

        Task<User> GetUserById(int id);

        Task<User> GetUserByLogin(string login);

        Task<User> GetUserByLoginAndPassword(string login, string password);

        Task UpdateUser(User user);

        Task InsertUser(User user);
    }
}
