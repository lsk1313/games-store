﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GamesStore.Entities.Domain;

namespace GamesStore.RepositoryInterfaces.ObjectInterfaces
{
    public interface ICategoryRepository
    {
        Task<IEnumerable<Category>> GetCategories();

        Task DeleteCategory(int id);

        Task<Category> GetCategory(int id);

        Task UpdateCategory(Category category);

        Task InsertCategory(Category category);
    }
}

