﻿using System.Threading.Tasks;
using AutoMapper;
using GamesStore.PresentationLayer.ViewModels;
using GamesStore.ServiceInterfaces.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace GamesStore.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : GenericController
    {
        public AccountController(IServiceFactory serviceFactory, 
            IMapper mapper) : base(serviceFactory, mapper)
        {
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> Login()
        {
            var user = await ServiceFactory.UserService.GetUserByLogin(UserInfo().Login);
            var userViewModel = Mapper.Map<UserViewModel>(user);

            return Ok(userViewModel);
        }
    }
}