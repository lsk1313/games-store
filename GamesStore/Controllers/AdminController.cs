﻿using System.Collections.Generic;
using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AutoMapper;
using GamesStore.Entities.DataTransferObjects;
using GamesStore.PresentationLayer.ViewModels;
using GamesStore.ServiceInterfaces.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace GamesStore.Controllers
{
    [Route("api/[controller]")]
    public class AdminController : GenericController
    {
        private readonly IWebHostEnvironment _hostingEnvironment;
        public AdminController(IServiceFactory serviceFactory, IMapper mapper,
            IWebHostEnvironment hostingEnvironment) : base(serviceFactory, mapper)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetGames()
        {
            var games = await ServiceFactory.GameService.GetGames();
            var gamesViewModel = Mapper.Map<IEnumerable<GameViewModel>>(games);

            return Ok(gamesViewModel);
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetCategories()
        {
            var categories = await ServiceFactory.CategoryService.GetCategories();
            var categoryViewModel = Mapper.Map<IEnumerable<CategoryViewModel>>(categories);

            return Ok(categoryViewModel);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> UpdateGame([FromBody] GameViewModel gameViewModel)
        {
            var gameDto = Mapper.Map<GameDto>(gameViewModel);

            if (await ServiceFactory.GameService.GetGameById(gameDto.Id) != null)
            {
                await ServiceFactory.GameService.UpdateGame(gameDto);
            }
            else
            {
                await ServiceFactory.GameService.InsertGame(gameDto);
            }

            return Ok(gameViewModel);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> DeleteGame(int gameId)
        {
            await ServiceFactory.GameService.DeleteGame(gameId);

            return Ok();
        }

        [HttpPost, DisableRequestSizeLimit]
        public async Task<IActionResult> UploadFile()
        {
            const string folderName = "Images";
            var path = string.Empty;

            try
            {
                var file = Request.Form.Files[0];
                var newPath = Path.Combine(_hostingEnvironment.WebRootPath, folderName);

                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }

                if (file.Length > 0)
                {
                    var typeFile = file.ContentType.Split('/')[1];
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).Name
                                       .TrimStart('"').Replace('"', '.') + typeFile;
                    var fullPath = Path.Combine(newPath, fileName);

                    if (System.IO.File.Exists(fullPath))
                    {
                        System.IO.File.Delete(fullPath);
                    }

                    await using var stream = new FileStream(fullPath, FileMode.Create);
                    await file.CopyToAsync(stream);
                    path = $"/{folderName}/{fileName}";

                    return Json(path);
                }
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Json(path);
        }
    }
}
