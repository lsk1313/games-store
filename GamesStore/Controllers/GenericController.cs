﻿using System.Net.Http.Headers;
using System.Security.Claims;
using AutoMapper;
using GamesStore.PresentationLayer.ViewModels;
using GamesStore.ServiceInterfaces.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;

namespace GamesStore.Controllers
{
    public class GenericController : Controller
    {
        protected readonly IServiceFactory ServiceFactory;

        protected readonly IMapper Mapper;

        public GenericController(IServiceFactory serviceFactory, IMapper mapper)
        {
            ServiceFactory = serviceFactory;

            Mapper = mapper;
        }

        [NonAction]
        protected UserClaimsViewModel UserInfo() => new UserClaimsViewModel
            {
                Login = GetClaims().Identity.Name,
            };

        [NonAction]
        private ClaimsPrincipal GetClaims()
        {
            var authenticationHeaderValue = AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]);
            var claims = ServiceFactory.TokenService.GetClaims(authenticationHeaderValue.Parameter);

            return claims;
        }
    }
}