﻿using System.Threading.Tasks;
using AutoMapper;
using GamesStore.PresentationLayer.ViewModels;
using GamesStore.ServiceInterfaces.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace GamesStore.Controllers
{
    [Route("api/[controller]")]
    public class TokenController : GenericController
    {
        public TokenController(IServiceFactory serviceFactory, IMapper mapper) : base(serviceFactory, mapper)
        {
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> CreateToken([FromBody] UserViewModel userViewModel)
        {
            var result =
                await ServiceFactory.TokenService.CreateJwtSecurityToken(userViewModel.Login, userViewModel.Password);

            return Ok(result);
        }
    }
}