import { CategoryModel } from "./category.model";

export class GameModel {
    id: number;

    name: string;

    description: string;

    price: number;

    imageName: string;

    imageSource: string;

    selectedCategories: CategoryModel[];

}
