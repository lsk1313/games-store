import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler } from "@angular/common/http";
import { Observable } from "rxjs";

import { AuthenticationService } from "./authentication.service";

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {
    constructor(public authentication: AuthenticationService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${this.authentication.getToken()}`
        }
      });

      return next.handle(req);
    }

}
