import { Injectable } from '@angular/core';
import { GameModel } from "../models/game.model";


@Injectable({
    providedIn: 'root'
})
export class EditGameService {

    private data: GameModel;

    setData(data) {
        this.data = data;
    }

    getData() {
        const temp = this.data;
        this.clearData();

        return temp;
    }

    clearData() {
        this.data = undefined;
    }
}
