import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router } from "@angular/router";

import { map } from 'rxjs/operators';

import { UserModel } from "../models/user.model";

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {
    tokenResult: any;

    constructor(private http: HttpClient, private router: Router ) { }

    signIn(login: UserModel) {

        const httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json' })
        };

        const userViewModel = JSON.stringify(login);

        return this.http.post('api/Token/CreateToken', userViewModel, httpOptions).pipe(map(result => {
            this.tokenResult = result;
            if (this.tokenResult && this.tokenResult.jwtToken && this.tokenResult.jwtToken.token) {
                sessionStorage.setItem('tokenInfo', JSON.stringify(this.tokenResult.jwtToken));
                this.authenticate();
            }

            return this.tokenResult;
        }));
    }

    authenticate() {
        this.http.get('api/Account/Login').subscribe(result => {
            if (result) {
                sessionStorage.setItem('userInfo', JSON.stringify(result));
                this.router.navigate(['/admin']);
            }
        });
    }

    signOut() {
        sessionStorage.removeItem("userInfo");
    }

    isLoggedIn() {
        return !(sessionStorage.getItem("userInfo") == null);
    }

    getToken() {
        const jwtToken = JSON.parse(sessionStorage.getItem('tokenInfo'));
        if (jwtToken) {
            return jwtToken.token;
        }

        return null;
    }
}
