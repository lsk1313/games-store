import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpRequest, HttpParams } from "@angular/common/http";

import { GameModel } from "../models/game.model";

@Injectable({
    providedIn: 'root'
})
export class GameService {

    constructor(private http: HttpClient) { }

    getGames() {
        return this.http.get('api/Admin/GetGames');
    }

    getCategories() {
        return this.http.get('api/Admin/GetCategories');
    }

    updateGame(game: GameModel) {

        const httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json' })
        };
        const gameViewModel = JSON.stringify(game);

        return this.http.post('api/Admin/UpdateGame', gameViewModel, httpOptions);
    }

    deleteGame(gameId: number) {
        const params = new HttpParams().set("gameId", gameId.toString());

        return this.http.post('api/Admin/DeleteGame', params);
    }

    uploadFile(formData: FormData) {
        const uploadReq = new HttpRequest('POST',
            `api/Admin`,
            formData,
            {
                reportProgress: true,
            });

        return this.http.request(uploadReq);
    }
}
