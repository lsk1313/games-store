import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { UserModel } from "../models/user.model";


@Injectable({ providedIn: 'root' })

export class AuthGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        const userIsAuthenticate = JSON.parse(sessionStorage.getItem('userInfo'));
        const info = userIsAuthenticate as UserModel;

        if (info) {

            return true;
        }
        this.router.navigate(['/signin'], { queryParams: { returnUrl: state.url } });

        return false;
    }
}
