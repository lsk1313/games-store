import { Component } from '@angular/core';

import { AuthenticationService } from "../shared/services/authentication.service";

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {

  constructor(private authenticationService: AuthenticationService) { }

  isExpanded = false;

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }

  logout() {
    this.authenticationService.signOut();
  }

  isLoggedIn() {
    return this.authenticationService.isLoggedIn();
  }

}
