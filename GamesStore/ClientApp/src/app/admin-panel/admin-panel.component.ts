import { Component, OnInit, Inject, Input } from '@angular/core';
import { Router } from "@angular/router";

import { EditGameService } from "../shared/services/edit-game.service";
import { GameService } from "../shared/services/game.service";
import { GameModel } from "../shared/models/game.model";

@Component({
    selector: 'app-admin-panel',
    templateUrl: './admin-panel.component.html',
    styleUrls: ['./admin-panel.component.css']
})

export class AdminPanelComponent implements OnInit {
    games: GameModel[] = [];

    constructor(private gameService: GameService,
        private router: Router,
        private editGameService: EditGameService) { }

    ngOnInit() {
        this.getGames();
    }

    getGames() {
        this.gameService.getGames().subscribe((result: GameModel[]) => {
            if (result) {
                this.games = result;
            }
        });
    }

    editGame(game: GameModel) {
        this.editGameService.setData(game);
        this.router.navigate(['/edit']);
    }

    addGame() {
        this.router.navigate(['/edit']);
    }

    deleteGame(game: GameModel) {
        if (confirm(`Delete this game: "${game.name}"?`)) {
            this.gameService.deleteGame(game.id).subscribe(() => {
                this.games = this.games.filter(item => item !== game);
            });
        }
        return;
    }
}
