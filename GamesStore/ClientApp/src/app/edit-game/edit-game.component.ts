import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from "@angular/forms";
import { Router } from "@angular/router";
import { HttpEventType } from '@angular/common/http';

import { GameService } from "../shared/services/game.service";
import { GameModel } from "../shared/models/game.model";
import { EditGameService } from "../shared/services/edit-game.service";
import { CategoryModel } from "../shared/models/category.model";

@Component({
    selector: 'app-edit-game',
    templateUrl: './edit-game.component.html',
    styleUrls: ['./edit-game.component.css']
})

export class EditGameComponent implements OnInit {
    game = this.editGameService.getData();

    gameForm: FormGroup;

    imgSource: string = null;

    categories: CategoryModel[];

    selectedCategories: CategoryModel[];

    constructor(
        private gameService: GameService,
        private router: Router,
        private fb: FormBuilder,
        private editGameService: EditGameService) {
    }

    ngOnInit() {

        //this.getCategories();
        this.categories = [
            { id: "1", categoryName: 'Action' },
            { id: "2", categoryName: 'Adventure' },
            { id: "3", categoryName: 'Indie' },
            { id: "4", categoryName: 'Massively Multiplayer' },
            { id: "5", categoryName: 'Casual' },
            { id: "6", categoryName: 'Racing' },
            { id: "7", categoryName: 'RPG' },
            { id: "8", categoryName: 'Simulation' },
            { id: "9", categoryName: 'Sports' },
            { id: "10", categoryName: 'Strategy' }
        ];

        this.selectedCategories = this.game && this.game.selectedCategories
            ? this.game.selectedCategories
            : new Array(this.categories.length);

        const formControls = this.categories.map((category: CategoryModel) => this.selectedCategories.find(x => JSON.stringify(x) === JSON.stringify(category))
            ? new FormControl(true) : new FormControl(false));


        this.gameForm = this.fb.group({
            id: new FormControl(this.game ? this.game.id : null),
            name: new FormControl(this.game ? this.game.name : '', Validators.compose([Validators.required])),
            description: new FormControl(this.game ? this.game.description : '', Validators.compose([Validators.required])),
            imageName: new FormControl(this.game ? this.game.imageName : '', Validators.compose([Validators.required])),
            imageSource: new FormControl(this.game ? this.game.imageSource : null),
            categories: new FormArray(formControls),
            price: new FormControl(this.game ? this.game.price : '', Validators.compose([Validators.required, Validators.pattern("^[0-9]*[.]?[0-9]+$")])),
        });

    }

    onSubmit() {
        if (this.gameForm.valid) {

            this.game = this.gameForm.value;

            this.setImageSource();

            this.setCategories();

            this.updateGame();
        } else {
            alert("Form is invalid!");
            return;
        }
    }

    updateGame() {
        this.gameService.updateGame(this.game).subscribe(() => {
            this.router.navigate(['/admin']);
        });
    }

    upload(files, fileName: string) {
        if (files.length === 0) return;

        const formData = new FormData();

        for (let file of files)
            formData.append(fileName, file);

        this.gameService.uploadFile(formData).subscribe(event => {
            if (event.type === HttpEventType.Response) {
                this.imgSource = event.body.toString();

                this.game.imageSource = this.imgSource;
            }
        });
    }

    deleteImage() {
        this.game.imageSource = null;
    }

    getCategories() {
        this.gameService.getCategories().subscribe((result: CategoryModel[]) => {
            if (result) {
                return result;
            }
        });

        return null;
    }

    private setImageSource() {
        if (this.imgSource) {
            this.game.imageSource = this.imgSource;
        }
    }

    private setCategories() {
        this.game.selectedCategories = this.gameForm.value.categories
            .map((checked, index) => checked ? this.categories[index] : null)
            .filter(value => value !== null);
    }
}
