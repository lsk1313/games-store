import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { AuthGuard } from "./shared/guard/auth-guard";
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { EditGameComponent } from './edit-game/edit-game.component';
import { TokenInterceptorService } from "./shared/services/token-interceptor.service";
import { PageNotBeFoundComponent } from './page-not-be-found/page-not-be-found.component';
import { FooterComponent } from './footer/footer.component';
import { CartComponent } from './cart/cart.component';

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        HomeComponent,
        SignInComponent,
        AdminPanelComponent,
        EditGameComponent,
        PageNotBeFoundComponent,
        FooterComponent,
        CartComponent,

    ],
    imports: [
        BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,

        RouterModule.forRoot([
            { path: '', component: HomeComponent, pathMatch: 'full' },
            { path: 'cart', component: CartComponent },

            { path: 'signin', component: SignInComponent },
            { path: 'admin', component: AdminPanelComponent, canActivate: [AuthGuard] },
            { path: 'edit', component: EditGameComponent, canActivate: [AuthGuard] },

            { path: '**', component: PageNotBeFoundComponent },

        ])
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true },
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
