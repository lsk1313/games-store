import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-not-be-found',
  templateUrl: './page-not-be-found.component.html',
  styleUrls: ['./page-not-be-found.component.css']
})
export class PageNotBeFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
