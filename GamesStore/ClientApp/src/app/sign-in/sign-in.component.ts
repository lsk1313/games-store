import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { UserModel } from "../shared/models/user.model";
import { AuthenticationService } from "../shared/services/authentication.service";



@Component({
    selector: 'app-sign-in',
    templateUrl: './sign-in.component.html',
    styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
    user: UserModel;
    userForm: FormGroup;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private authenticationService: AuthenticationService
    ) { }

    ngOnInit() {
        this.user = new UserModel();

        this.userForm = this.fb.group({
            email: new FormControl('', Validators.compose([Validators.required, Validators.email])),
            password: new FormControl('', Validators.compose([Validators.required, Validators.minLength(4)])),
        });
    }

    onSubmit() {
        if (this.userForm.invalid) {
            alert(`Please enter email or password`);
            return;
        }

        this.signIn();
    }

    signIn() {
        this.authenticationService.signIn(this.user).subscribe((result) => {
            if (result.error) {
                alert(`Authenticate error: ${result.error}`);
            }
        });
    }
}
