﻿using GamesStore.ServiceInterfaces.Interfaces;

namespace GamesStore.BusinessLogicLayer.Services
{
    public class ServiceFactory : IServiceFactory
    {
        public ServiceFactory(IUserService userService, IGameService gameService, ITokenService tokenService,
            ISecurityService securityService, ICategoryService categoryService)
        {
            UserService = userService;
            GameService = gameService;
            TokenService = tokenService;
            SecurityService = securityService;
            CategoryService = categoryService;
        }

        public IUserService UserService { get; }
        public IGameService GameService { get; }
        public ITokenService TokenService { get; }
        public ISecurityService SecurityService { get; }
        public ICategoryService CategoryService { get; }

    }
}
