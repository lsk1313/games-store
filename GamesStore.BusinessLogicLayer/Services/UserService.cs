﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using GamesStore.Entities.DataTransferObjects;
using GamesStore.Entities.Domain;
using GamesStore.RepositoryInterfaces.UoW;
using GamesStore.ServiceInterfaces.Interfaces;

namespace GamesStore.BusinessLogicLayer.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;

        public UserService(IUnitOfWork uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }

        public async Task<UserDto> GetUserByLogin(string login)
        {
            var user = await _uow.UserRepository.GetUserByLogin(login);

            return _mapper.Map<UserDto>(user);
        }

        public async Task<UserDto> GetUserByLoginAndPassword(string login, string password)
        {
            var user = await _uow.UserRepository.GetUserByLoginAndPassword(login, password);

            return _mapper.Map<UserDto>(user);
        }

        public async Task<UserDto> GetUserById(int id)
        {
            var user = await _uow.UserRepository.GetUserById(id);

            return _mapper.Map<UserDto>(user);
        }

        public async Task InsertUser(UserDto userDto)
        {
            var user = _mapper.Map<User>(userDto);

            await _uow.UserRepository.InsertUser(user);
        }

        public async Task UpdateUser(UserDto userDto)
        {
            var user = _mapper.Map<User>(userDto);

            await _uow.UserRepository.UpdateUser(user);
        }

        public async Task<IEnumerable<UserDto>> GetUsers()
        {
            var users = await _uow.UserRepository.GetUsers();

            return _mapper.Map<IEnumerable<UserDto>>(users);
        }
    }
}
