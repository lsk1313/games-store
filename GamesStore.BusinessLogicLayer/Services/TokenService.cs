﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using GamesStore.Entities.JWT;
using GamesStore.RepositoryInterfaces.UoW;
using GamesStore.ServiceInterfaces.Interfaces;
using Microsoft.IdentityModel.Tokens;

namespace GamesStore.BusinessLogicLayer.Services
{
    public class TokenService : ITokenService
    {
        private readonly IUnitOfWork _uow;

        private readonly ISecurityService _securityService;

        public TokenService(IUnitOfWork uow, ISecurityService securityService)
        {
            _uow = uow;
            _securityService = securityService;
        }

        public async Task<JwtResult> CreateJwtSecurityToken(string login, string password)
        {
            var jwtResult = new JwtResult();
            var securityPassword = _securityService.GetSha256Hash(password);
            var user = await _uow.UserRepository.GetUserByLoginAndPassword(login, securityPassword);

            if (user == null)
            {
                jwtResult.Error = "Invalid username or password";
                return jwtResult;
            }

            var claims = new[]
            {
                new Claim(ClaimTypes.Name, user.Login),

            };

            var claimsIdentity = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, null);
            var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(JwtConst.KEY));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                issuer: JwtConst.ISSUER,
                audience: JwtConst.AUDIENCE,
                claims: claims,
                expires: DateTime.Now.AddMinutes(JwtConst.LIFETIME),
                signingCredentials: credentials);
            var finalToken = new JwtSecurityTokenHandler().WriteToken(token);

            jwtResult.JwtToken = new JwtToken
            {
                Token = finalToken,
                Login = claimsIdentity.Name
            };

            return jwtResult;
        }

        public ClaimsPrincipal GetClaims(string token)
        {
            var securityTokenHandler = new JwtSecurityTokenHandler();

            var validateParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = JwtConst.ISSUER,
                ValidateAudience = true,
                ValidAudience = JwtConst.AUDIENCE,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(JwtConst.KEY))
            };

            var claimsPrincipal = securityTokenHandler.ValidateToken(token, validateParameters, out _);

            return claimsPrincipal;
        }
    }
}
