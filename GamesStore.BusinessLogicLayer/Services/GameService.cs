﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using GamesStore.Entities.DataTransferObjects;
using GamesStore.Entities.Domain;
using GamesStore.RepositoryInterfaces.UoW;
using GamesStore.ServiceInterfaces.Interfaces;

namespace GamesStore.BusinessLogicLayer.Services
{
    public class GameService : IGameService
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;

        public GameService(IUnitOfWork uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }

        public async Task<GameDto> GetGameByName(string name)
        {
            var game = await _uow.GameRepository.GetGameByName(name);

            return _mapper.Map<GameDto>(game);
        }

        public async Task<GameDto> GetGameById(int id)
        {
            var game = await _uow.GameRepository.GetGameById(id);

            return _mapper.Map<GameDto>(game);
        }

        public async Task UpdateGame(GameDto gameDto)
        {
            var game = _mapper.Map<Game>(gameDto);
            await _uow.GameRepository.UpdateGame(game);
        }

        public async Task InsertGame(GameDto gameDto)
        {
            var game = _mapper.Map<Game>(gameDto);
            await _uow.GameRepository.InsertGame(game);
        }

        public async Task<IEnumerable<GameDto>> GetGames()
        {
            var games = await _uow.GameRepository.GetGames();

            return _mapper.Map<IEnumerable<GameDto>>(games);
        }

        public async Task DeleteGame(int id)
        {
            await _uow.GameRepository.DeleteGame(id);
        }
    }
}
