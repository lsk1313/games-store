﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using GamesStore.Entities.DataTransferObjects;
using GamesStore.Entities.Domain;
using GamesStore.RepositoryInterfaces.UoW;
using GamesStore.ServiceInterfaces.Interfaces;

namespace GamesStore.BusinessLogicLayer.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;

        public CategoryService(IUnitOfWork uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }

        public async Task DeleteCategory(int id)
        {
            await _uow.CategoryRepository.DeleteCategory(id);
        }

        public async Task<IEnumerable<CategoryDto>> GetCategories()
        {
            var categories = await _uow.CategoryRepository.GetCategories();

            return _mapper.Map<IEnumerable<CategoryDto>>(categories);
        }

        public async Task<CategoryDto> GetCategoryById(int id)
        {
            var game = await _uow.CategoryRepository.GetCategory(id);

            return _mapper.Map<CategoryDto>(game);
        }

        public async Task InsertCategory(CategoryDto categoryDto)
        {
            var category = _mapper.Map<Category>(categoryDto);
            await _uow.CategoryRepository.InsertCategory(category);
        }

        public async Task UpdateCategory(CategoryDto categoryDto)
        {
            var category = _mapper.Map<Category>(categoryDto);
            await _uow.CategoryRepository.UpdateCategory(category);
        }
    }
}
