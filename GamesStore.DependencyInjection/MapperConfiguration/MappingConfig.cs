﻿using System.Linq;
using AutoMapper;
using GamesStore.Entities.DataTransferObjects;
using GamesStore.Entities.Domain;
using GamesStore.PresentationLayer.ViewModels;

namespace GamesStore.DependencyInjection.MapperConfiguration
{
    public class MappingConfig : Profile
    {
        public MappingConfig()
        {
            #region dto to domain

            CreateMap<UserDto, User>()
                .ForMember(s => s.Login, x => x.MapFrom(z => z.Login))
                .ForMember(s => s.Password, x => x.MapFrom(z => z.Password));

            CreateMap<GameDto, Game>()
                .ForMember(s => s.Id, x => x.MapFrom(z => z.Id))
                .ForMember(s => s.Name, x => x.MapFrom(z => z.Name))
                .ForMember(s => s.Description, x => x.MapFrom(z => z.Description))
                .ForMember(s => s.ImageName, x => x.MapFrom(z => z.ImageName))
                .ForMember(s => s.ImageSource, x => x.MapFrom(z => z.ImageSource))
                .ForMember(s => s.Price, x => x.MapFrom(z => z.Price))
                .ForMember(s => s.Categories, x => x.MapFrom(z => z.Categories));

            CreateMap<CategoryDto, Category>()
                .ForMember(s => s.Id, x => x.MapFrom(z => z.Id))
                .ForMember(s => s.CategoryName, x => x.MapFrom(z => z.CategoryName))
                .ForMember(s => s.Games, x => x.MapFrom(z => z.Games));

            #endregion

            #region domain to dto

            CreateMap<User, UserDto>()
                .ForMember(s => s.Login, x => x.MapFrom(z => z.Login))
                .ForMember(s => s.Password, x => x.MapFrom(z => z.Password));

            CreateMap<Game, GameDto>()
                .ForMember(s => s.Id, x => x.MapFrom(z => z.Id))
                .ForMember(s => s.Name, x => x.MapFrom(z => z.Name))
                .ForMember(s => s.Description, x => x.MapFrom(z => z.Description))
                .ForMember(s => s.ImageName, x => x.MapFrom(z => z.ImageName))
                .ForMember(s => s.ImageSource, x => x.MapFrom(z => z.ImageSource))
                .ForMember(s => s.Price, x => x.MapFrom(z => z.Price))
                .ForMember(s => s.Categories, x => x.MapFrom(z => z.Categories));

            CreateMap<Category, CategoryDto>()
                .ForMember(s => s.Id, x => x.MapFrom(z => z.Id))
                .ForMember(s => s.CategoryName, x => x.MapFrom(z => z.CategoryName))
                .ForMember(s => s.Games, x => x.MapFrom(z => z.Games));

            #endregion

            #region dto to view model

            CreateMap<UserDto, UserViewModel>()
                .ForMember(s => s.Login, x => x.MapFrom(z => z.Login))
                .ForMember(s => s.Password, x => x.MapFrom(z => z.Password));

            CreateMap<GameDto, GameViewModel>()
                .ForMember(s => s.Name, x => x.MapFrom(z => z.Name))
                .ForMember(s => s.Description, x => x.MapFrom(z => z.Description))
                .ForMember(s => s.ImageName, x => x.MapFrom(z => z.ImageName))
                .ForMember(s => s.ImageSource, x => x.MapFrom(z => z.ImageSource))
                .ForMember(s => s.Price, x => x.MapFrom(z => z.Price))
                .ForMember(s => s.SelectedCategories, x => x.MapFrom(z => z.Categories.ToArray()));

            CreateMap<CategoryDto, CategoryViewModel>()
                .ForMember(s => s.CategoryName, x => x.MapFrom(z => z.CategoryName));
            #endregion

            #region view model to dto

            CreateMap<UserViewModel, UserDto>()
                .ForMember(s => s.Login, x => x.MapFrom(z => z.Login))
                .ForMember(s => s.Password, x => x.MapFrom(z => z.Password));

            CreateMap<GameViewModel, GameDto>()
                .ForMember(s => s.Name, x => x.MapFrom(z => z.Name))
                .ForMember(s => s.Description, x => x.MapFrom(z => z.Description))
                .ForMember(s => s.ImageName, x => x.MapFrom(z => z.ImageName))
                .ForMember(s => s.ImageSource, x => x.MapFrom(z => z.ImageSource))
                .ForMember(s => s.Price, x => x.MapFrom(z => z.Price))
                .ForMember(s => s.Categories, x => x.MapFrom(z => z.SelectedCategories.ToList()));

            CreateMap<CategoryViewModel, CategoryDto>()
                .ForMember(s => s.CategoryName, x => x.MapFrom(z => z.CategoryName));
            #endregion
        }
    }
}