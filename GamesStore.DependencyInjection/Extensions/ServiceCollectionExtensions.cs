﻿using System;
using AutoMapper;
using GamesStore.BusinessLogicLayer.Services;
using GamesStore.DataAccessLayer.Context;
using GamesStore.DataAccessLayer.Repository;
using GamesStore.DataAccessLayer.UoW;
using GamesStore.RepositoryInterfaces.ObjectInterfaces;
using GamesStore.RepositoryInterfaces.UoW;
using GamesStore.ServiceInterfaces.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace GamesStore.DependencyInjection.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void RegisterServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<AppDbContext>(options => options
                .UseSqlServer(configuration
                    .GetConnectionString("DefaultConnection"), x => x
                    .MigrationsAssembly("GamesStore.DataAccessLayer")));

            services.AddTransient<IUnitOfWork, UnitOfWork>(provider=> new UnitOfWork(configuration
                .GetConnectionString("DefaultConnection")));
            services.AddSingleton<IUserRepository, UserRepository>();
            services.AddSingleton<IGameRepository, GameRepository>();
            services.AddSingleton<ICategoryRepository, CategoryRepository>();

            services.AddTransient<IServiceFactory, ServiceFactory>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IGameService, GameService>();
            services.AddTransient<ICategoryService, CategoryService>();

            services.AddTransient<ITokenService, TokenService>();
            services.AddTransient<ISecurityService, SecurityService>();

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

        }
    }
}
