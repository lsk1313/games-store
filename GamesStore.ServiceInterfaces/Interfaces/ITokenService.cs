﻿using System.Security.Claims;
using System.Threading.Tasks;
using GamesStore.Entities.JWT;

namespace GamesStore.ServiceInterfaces.Interfaces
{
    public interface ITokenService
    {
        Task<JwtResult> CreateJwtSecurityToken(string login, string password);

        ClaimsPrincipal GetClaims(string token);
    }
}
