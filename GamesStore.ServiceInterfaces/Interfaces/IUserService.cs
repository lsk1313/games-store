﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GamesStore.Entities.DataTransferObjects;

namespace GamesStore.ServiceInterfaces.Interfaces
{
    public interface IUserService
    {
        Task<UserDto> GetUserByLogin(string login);

        Task<UserDto> GetUserByLoginAndPassword(string login, string password);

        Task<UserDto> GetUserById(int id);

        Task UpdateUser(UserDto userDto);

        Task InsertUser(UserDto userDto);

        Task<IEnumerable<UserDto>> GetUsers();

    }
}
