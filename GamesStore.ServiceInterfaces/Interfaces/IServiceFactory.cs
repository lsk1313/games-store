﻿
namespace GamesStore.ServiceInterfaces.Interfaces
{
    public interface IServiceFactory
    {
        IUserService UserService { get; }

        IGameService GameService { get; }

        ITokenService TokenService { get; }

        ISecurityService SecurityService { get; }

        ICategoryService CategoryService { get; }

    }
}
