﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GamesStore.Entities.DataTransferObjects;

namespace GamesStore.ServiceInterfaces.Interfaces
{
    public interface IGameService
    {
        Task<GameDto> GetGameByName(string name);

        Task<GameDto> GetGameById(int id);

        Task InsertGame(GameDto gameDto);

        Task UpdateGame(GameDto gameDto);

        Task<IEnumerable<GameDto>> GetGames();

        Task DeleteGame(int id);
    }
}
