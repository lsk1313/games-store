﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GamesStore.Entities.DataTransferObjects;

namespace GamesStore.ServiceInterfaces.Interfaces
{
    public interface ICategoryService
    {
        Task<CategoryDto> GetCategoryById(int id);

        Task InsertCategory(CategoryDto categoryDto);

        Task UpdateCategory(CategoryDto categoryDto);

        Task<IEnumerable<CategoryDto>> GetCategories();

        Task DeleteCategory(int id);
    }
}
